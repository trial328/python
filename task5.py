import csv
import os

##CREATE
with open('task5.csv','w') as f:
    pass

##READ
with open('task5.csv','r') as f:
    print(f.read())

##UPDATE
lis=[[2,3],[3,4],[4,5]]
with open('task5.csv','w') as f:
    writer = csv.writer(f)
    writer.writerow(lis)

##DELETE
os.remove('task5.csv')