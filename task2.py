
## DECLARING STRING VARIABLES
file1="The articles and tutorials in this section contain best practices and other “nuggets of wisdom” to help your write better, more idiomatic, and more Pythonic code. Here you’ll find specific resources that will teach you how to idiomatically use the features of Python, what sets it apart, and how writing in Python is different than writing code in another language. As a high-level programming language, Python offers a lot of flexibility and freedom to implement thing. This can make it challenging to pick and choose from “the right way” to do things if there are several viable approaches you can choose from. Our goal here is to make you a better—more effective, more knowledgeable, more practical—Python developer. You’ll get the most out of these article if you already have some knowledge of Python, and you want to get to the next level."
file2="The articles and tutorials in this section contain best practices and other “nuggets of wisdom” to help your write better, more idiomatic, and more Pythonic code. Here you’ll find specific resources that will teach you how to idiomatically use the features of Python, what sets it apart, and how writing in Python is different than writing code in another language. As a high-level programming language, Python offers a lot of flexibility and freedom to implement thing. This can make it challenging to pick and choose from “the right way” to do things if there are several viable approaches you can choose from. Our goal here is to make you a better—more effective, more knowledgeable, more practical—Python developer. You’ll get the most out of these article if you already have some knowledge of Python, and you want to get to the next level."

## CHECK EQUALITY OF BOTH FILES
if file1==file2:
    print ("Equal")
else:
    print ("not equal")

## CHANGING CASE OF FILE2 TO UPPER
file2=file2.upper()

## CHECK EQUALITY OF BOTH FILES
if file1==file2:
    print ("Equal")
else:
    print ("not equal")

##CHANGE BOTH FILES TO SIMILAR CASE
if file1!=file2:
    file1=file1.casefold()
    file2=file2.casefold()
else:
    print("ALREADY EQUAL!")
## CHECK CASE OF BOTH FILES
file1
file2

## CHECK EQUALITY
if file1==file2:
    print ("Equal")
else:
    print ("not equal")