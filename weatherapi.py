#WEATHER API
import requests, json

api_key = "e2314f789baa1493ac5e0afb28328167"

wurl = "https://api.openweathermap.org/data/2.5/weather?"


city = input("Enter the City : ")
fin_url = wurl + "q=" + city + "&appid=" + api_key

#get response from url
response = requests.get(fin_url)

# put json format response data in 'x' which makes it a dictionary
x = response.json()

# tranverse through the response dictionary to o/p weather data according to our needs
if x["cod"] != 404:

    y = x["main"]

    ctemp = y["temp"]
    cctemp = int(ctemp)
    cctemp = (cctemp-273.15)

    cpress = y["pressure"]

    chumid = y["humidity"]
 
    z = x["weather"]
 
    wdes = z[0]["description"]

    cc = x["sys"]
    county = cc["country"]
 
    # print following values
    print(" Country = " + county +
          "\n Temperature = " +
                    str(round(cctemp,2)) + " Celsius" +
          "\n Atmospheric pressure (in hPa unit) = " +
                    str(cpress) +
          "\n Humidity  = " +
                    str(chumid) + "%" +
          "\n Description = " +
                    str(wdes))
 
else:
    print(" City Not Found ")
