import csv
from datetime import timezone
import datetime

# Getting the current date and time
dt = datetime.datetime.now(timezone.utc)
#print(dt)
#convert our datetime to UTC using - tzinfo
utc_time = dt.replace(tzinfo=timezone.utc)
utc_timestamp = utc_time.timestamp()

dl=[['1','abcd'] ,['2','xyz']]
for x in dl:
    x.append(utc_timestamp)

#dl.append(utc_timestamp)

print("new list : ",dl)
with open('task4.csv','w') as f:
    writer = csv.writer(f)
    writer.writerow(dl)
    
    
    


